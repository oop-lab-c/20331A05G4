import java.util.*;

 class MyException extends Exception{
   MyException(int n){
    System.out.println("Natural Number should be greater than zero");
  }
}

class UserdefinedExc
{
  public static void main(String[] args){
   System.out.println("Enter Natural Number: ");
   Scanner sc = new Scanner(System.in);
   int a = sc.nextInt();
    if(a > 0){
      System.out.println(a + " is a Natural Number");
    }else{
       try{
        throw new MyException(a);
      }catch(Exception e){
       System.out.println(e);
      }finally{
        System.out.println("All Exceptions are successfully Handled...");
      }
    }
  }
}