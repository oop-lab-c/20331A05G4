#include<iostream>
using namespace std;


class MethodOverloading{
    public:
    void sum(int a ,int b ,int c){
        cout<<"Sum of a,b and c = "<<a+b+c<<endl;    
    }
    void sum(int a , int b){
        cout<<"Sum of a and b = "<<a+b<<endl;
    }
};
int main(){
    MethodOverloading obj1;
    obj1.sum(1,2,3);
    obj1.sum(10,20);
}