abstract class Parent{
    void display(){                    
        System.out.println("User..... I am Parent");
    }
    abstract void display1();       
}

class Child1 extends Parent{
    void display1(){                   
        System.out.println("User..... I am Child1");
    }
}
class ParAbs{
    public static void main(String[] args){
        Child1 obj1 = new Child1();
        obj1.display1();
        obj1.display();
    }
}