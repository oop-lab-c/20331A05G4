#include<iostream>
using namespace std;

class Parent{
         virtual void profession()=0;   
};
class Child1 : public Parent{
    public:
        void profession(){                      
            cout<<"Programmer "<<endl;
        }
};
class Child2 : public Parent{
    public:
        void profession(){              
            cout<<"Doctor "<<endl;
        }
};
int main(){
    Child1 son;
    Child2 daughter;
    cout<<"Child1 profession"<<endl;
    son.profession();                 
    cout<<"Child2 profession"<<endl;
    daughter.profession();    
    return 0;
}
