#include <iostream>
using namespace std;

template <class Template>
class Number {         
    Template num;
   public:
    Number(Template n){
       num = n;
    }  
    Template getNum() {
        return num;
    }
};

template <typename Sum>       
Sum add(Sum a, Sum b){
    return a+b;
}
int main() {

    Number<char> Obj1(57);
    Number<int> Obj2(47);
    cout << "char Number = " << Obj1.getNum() << endl;       
    cout << "int Number = " << Obj2.getNum() << endl;     
    cout << "float addition = " << add < float > (2.6 , 7.2)<<endl;
    cout << "int addition = " << add < int > (2 , 4) << endl; 
    return 0;
}
