#include<iostream>
using namespace std;

class Food{
    public:
    void vegetables(){                         
        cout<<"Vegetables :"<<endl;
    }
    virtual void fruits()=0;               
};
class Mango : public Food{
    public:
    void fruits(){
        cout<<"I am mango"<<endl;
    }
};
class Apple : public Food{
    public:
    void fruits(){
        cout<<"I am apple"<<endl;
    } 
};
int main(){
    Mango obj1;
    Apple obj2;
    cout<<"Child one function(mango) :"<<endl;
    obj1.fruits();               
    cout<<"Child two function(apple) :"<<endl;
    obj2.fruits();               
    return 0;
}