import java.util.*;
class Parent{
    void age(int age){                 
        System.out.println("Parent age : "+ age);
    }
}

class Child1 extends Parent{
    void age(int age){                  
        System.out.println("Child age : "+ age);
    }
}

class MethodOR{
   public static void main(String[] args){
       Scanner sc = new Scanner(System.in);
       System.out.println("Enter age : ");
       int age = sc.nextInt();
       Child1 obj1 = new Child1();
       obj1.age(age);                
   } 
}