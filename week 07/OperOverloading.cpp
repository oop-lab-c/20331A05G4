#include<iostream>
using namespace std;
 class OperOverloading{
     float real,imaginary;
     public:
     OperOverloading(){
            real=0;
            imaginary=0;
     }
     void access(){
         cout<<"Enter the values of real and imaginary parts of a complex number :"<<endl;
         cin>>real>>imaginary;
     }
     OperOverloading operator + (const OperOverloading &obj){         //Overloading constructor
         OperOverloading temp;
         temp.real = real + obj.real;
         temp.imaginary = imaginary + obj.imaginary;
         return temp;
     }
     void display(){
         if(imaginary>0)
            cout<<"Output "<<real<<"+"<<imaginary<<"i"<<endl;
        else    
            cout<<"Output "<<real<<imaginary<<"i"<<endl;
     }
 };
 int main(){
     OperOverloading obj1,obj2,obj3;
     cout<<"Enter two complex numbers "<<endl;
     obj1.access();
     obj2.access();
     obj3 = obj1 + obj2;
     obj3.display();
     return 0;
 }
