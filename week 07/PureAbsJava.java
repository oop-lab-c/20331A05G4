interface Parent{               
    void future();
}

class Child1 implements Parent{        
    public void future(){
        System.out.println("Child1 choose Programmer as future career ");
    }
}

class Child2 implements Parent{       
    public void future(){
        System.out.println("Child2 choose Doctor as future career ");
    }
}

class PureAbs{
    public static void main(String []args){
        Child1 obj1 = new Child1();
        Child2 obj2 = new Child2();
        obj1.future();
        obj2.future();
    }
}