import java.util.*;

class Parent{
    void add(){
        System.out.println("I am parent");
    }
}

class Child extends Parent{
    void add(int a, int b){
        System.out.println(a + b);
    }

    public static void main(String[] args){
        int a, b;
        Child obj1 = new Child();
        System.out.println("Enter Two Integers:");
        Scanner sc = new Scanner(System.in);
        a = sc.nextInt();
        b = sc.nextInt();
        obj1.add(a, b);
        Parent obj2 = new Parent();
        obj2.add();
    }
}
