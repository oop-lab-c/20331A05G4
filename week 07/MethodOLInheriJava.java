class Parent {
    void sum(String a , String b){          
        System.out.println(a+b);
    }
}
class Child1 extends Parent{
    void sum(int x,int y){                   
        System.out.println("Sum = " + (x+y));
    }
}
class MethodOLInheri{
    public static void main(String[] args){
        Child1 obj1 = new Child1();
        obj1.sum("Sanivada"," Venkata Rama Vivek Vardhan ");          
        obj1.sum(6,7);                       
    }
}