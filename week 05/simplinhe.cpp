//simple inheritence
#include<iostream>
using namespace std;
// parent class
class ArthOperations {
    public:
    void add (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"sum : "<<a+b<<endl;
    }
    void sub (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"subtraction : "<<a-b<<endl;
    }
};
// derived class
class Multip : public ArthOperations{
    public :
void multi (){
        float a,b;
        cout<<"Enter two numbers : "<<endl;
        cin>>a>>b;
        cout<<"Multiplication : "<<a*b<<endl;
    }
};
int main()
{
    Multip obj;
    obj.add();
    obj.multi();
}
