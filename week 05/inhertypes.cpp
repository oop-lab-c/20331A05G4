// By including more than one inheritence from the inheritences mentioned below will result hybrid inheritence 
#include<iostream>
using namespace std;
// parent_class
class Parent1 {
public:
    void displayp1(){
    cout<<"HELLO parent1 !"<<endl;
    }
};
class Parent2 {
public :
    void displayp2(){
    cout<<"Hello parent2 !"<<endl;
    }
};
// SIMPLE INHERITENCE
class Child1 : public Parent1{
    public :
    void displayc1()
    {
        cout<<"HELLO Child1 !"<<endl;
    }
};
// MULTIPLE INHERITENCE
class Child2 : public Parent1 , public Parent2 {
    public :
    void displayc2()
    {
        cout<<"HELLO Child2 !"<<endl;
    }
};
// MULTILEVEL INHERITENCE
class Child3 : public Child1{
    public:
    void displayc3()
    {
        cout<<"HELLO Child3 !"<<endl;
    }
};
//HIERARCHIAL INHERITENCE
class Child4 : public Parent2{
    public :
    void displayc4()
    {
        cout<<"HELLO Child4 !"<<endl;
    }
};
class Child5 : public Parent2{
    public :
void displayc5()
    {
        cout<<"HELLO Child1 !"<<endl;
    }
};
int main()
{
    Child1 objc1;
    Child2 objc2;
    Child3 objc3;
    Child4 objc4;
    Child5 objc5;
    cout<<"Simple Inheritence "<<endl;
    objc1.displayp1();
    cout<<"\nMultiple Inheritence "<<endl;
    objc2.displayp1();
    objc2.displayc2();
    objc2.displayp2();
    cout<<"\nMultilevel Inheritence "<<endl;
    objc3.displayc1();
    objc3.displayc3();
    cout<<"\nHierarchial Inheritence"<<endl;
    objc4.displayc4();
    objc4.displayp2();
    objc5.displayc5();
    objc5.displayp2();
}
