import java.util.Scanner;
class Parent {
    void name() {
        System.out.println("Hi S V R");
    }
}

class Child extends Parent {
    void welcome() {
        System.out.println("Welcome to MVGR");
    }

    public static void main(String[] args) {
        Child obj1 = new Child();
        obj1.name();
        obj1.welcome();
    }
}
