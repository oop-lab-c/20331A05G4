import java.lang.*;
class Animal
{
    void bark()
    {
        System.out.println("Barking");
    }
}
class Dog extends Animal
{
    void bark1()
    {
        System.out.println("bow..bow");
    }
}
class Puppy extends Dog // multiple inheitance 
{
    public static void main(String[] args)
    {
        Puppy obj1 = new Puppy();
        obj1.bark1();
        obj1.bark();
    }
}
class Cat extends Animal // simple inheritance
{
    void bark2()
    {
        System.out.println("meow..meow");
    }
    public static void main(String[] args)
    {
        Cat obj2 = new Cat();
        obj2.bark2();
        obj2.bark();
    }
}
