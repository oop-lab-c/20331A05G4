// public inheritence
#include<iostream>
using namespace std;
class Parent {
    private:
     int z = 1;
     int getpvt()
       {
           return z;
       }
    protected :
     int y = 2;
     public:
     int x = 3;

};
class Publicinherit : public Parent{
    public:
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
class Protectedinherit : protected Parent{
    public:
    int getpub(){
        return x;
        }
        void print()
        {
            cout<<"S V R Vivek Vardhan";
        }
    
    int getprot(){
        return y;
        }
    
};
class Privateinherit : private Parent{
    public:
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
int main(){
    Publicinherit obj;
    cout<<"PUBLIC INHERITENCE : \n";
    cout<<"Public : "<<obj.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<obj.getprot()<<endl;
    Protectedinherit objprot;
    cout<<"\nPROTECTED INHERITENCE : \n";
    cout<<"Public : "<<objprot.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<objprot.getprot()<<endl;
    objprot.print();
    Privateinherit objpvt;
    cout<<"\nPRIVATE INHERITENCE : \n";
    cout<<"Public : "<<objpvt.getpub()<<endl;
    cout<<"Private can't be accessed "<<endl;
    cout<<"Protected : "<<objpvt.getprot()<<endl;
    return 0;
}
