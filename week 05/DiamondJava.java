interface GrandParent  
{   
default void display()   
{   
System.out.println("Hi ");   
}   
}   
interface Parent1 extends GrandParent  
{   
 default void method1()
 {
     System.out.println("S V R...");
 }
}   
interface Parent2 extends GrandParent  
{  
  default void method2()
  {
      System.out.println("Vivek Vardhan...");
  }
}   
public class Child1 implements Parent1, Parent2  
{   
public static void main(String args[])   
{   
Child1 obj1 = new Child1();   
obj1.display(); 
obj1.method1();
obj1.method2();
}   
} 

