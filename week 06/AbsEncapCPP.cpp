#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
        int priVar;
    protected:
        int proVar;
    public:
      int pubVar;
    public:
    void setVar(int priValue,int proValue,int pubValue)
    {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
     public:
    void getVar()
    {
       cout<<"Private variable : "<<priVar <<endl;
       cout<<"Protected variable : "<<proVar <<endl;
       cout<<"Public variable : "<<pubVar <<endl;
    }
};
int main()
{
     AccessSpecifierDemo obj;
     obj.setVar(1 ,2 ,3);
     obj.getVar();
     return 0;
}
