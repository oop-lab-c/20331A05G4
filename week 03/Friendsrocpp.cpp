#include<iostream>
using namespace std;
class Box
{
    public:
    float width;
    float length;
    float height;
    friend void boxdimensions(Box obj1);
    void setboxdimensions(float l,float w,float h);
    void boxarea(float length,float width)
    {
        float area;
        area=length*width;
        cout<<"Area is "<<area<<endl;
    }
    void boxvolume(float ,float ,float);
    inline void displaymsg()
    {
        cout<<"Hi c++ "<<endl;
    }
};
void Box::setboxdimensions(float l,float w,float h)
{
    length = l;
    width = w;
    height = h;
}
void Box::boxvolume(float len,float wid,float height)
{
    float volume;
    volume = len*wid*height;
    cout<<"volume is "<<volume<<endl;
}
void boxdimensions(Box obj1)
{
    cout<<"the length is "<<obj1.length<<endl;
    cout<<"the width is "<<obj1.width<<endl;
    cout<<"the heigth is "<<obj1.height<<endl;
}
int main()
{
    float length,width,height;
    cout<<"enter the dimensions"<<endl;
    cin>>length>>width>>height;
    Box obj1;
    obj1.boxarea(length,width);
    obj1.setboxdimensions(length,width,height);
    obj1.boxvolume(length,width,height);
    boxdimensions(obj1);
    obj1.displaymsg();
    return 0;
}