import java.lang.*;
//import java.util.*;
class MyThread1 extends Thread {
    public void run() {
    System.out.println("Multiple of 2:");
       for(int i=0;i<6;i++){
        try{
            sleep(2000);
        }
        catch(Exception e){
            System.out.println(e);
        }
          System.out.println("Thread-A"+" "+i*2);
       }
    }
}
class MyThread2 extends Thread {
    public void run() {
        System.out.println("Multiple of 3:");
        for(int i=0;i<6;i++){
        System.out.println("Thread-B"+" "+i*3);
        }
    }
}
class Program1{
    public static void main(String args[])throws Exception{
        MyThread1 t1 = new MyThread1();
        MyThread2 t2 = new MyThread2();
        t1.start();
        t2.start();
        t1.setName("Thread-A");
        t2.setName("Thread-B");
        System.out.println("Name of the first thread"+t1.getName());
        System.out.println("Name of the second thread"+" "+t2.getName());
        System.out.println("Priority of Thread-A"+" "+t1.getPriority());
    }
}
