class MainThread{
    synchronized void run(int variable){
         for(int i=1;i<5;i++){
             System.out.println(variable*i);
             try{
                 Thread.sleep(2000);
            }catch(Exception e){
                 System.out.println(e);
            }
        }
    }
}
class Thread1 extends Thread{
    MainThread one;
    Thread1(MainThread obj){
        one=obj;
    }
    public void run(){
        one.run(10);
    }
}
class Thread2 extends Thread{
    MainThread two;
    Thread2(MainThread obj){
        two=obj;
    }
    public void run(){
        two.run(12);
    }
}

class ThreadSync{
    public static void main(String[] args){
        MainThread obj1 = new MainThread();
        Thread1 obj2 = new Thread1(obj1);
        Thread2 obj3 = new Thread2(obj1);
        obj2.start();
        obj3.start();
    }
}
