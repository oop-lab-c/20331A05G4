import java.lang.*;
//import java.util.*;
class MyThread1 implements Runnable{
    public void run() {
    System.out.println("Multiple of 2:");
       for(int i=0;i<6;i++){
          System.out.println("Thread-A"+" "+i*2);
       }
    }
}
class MyThread2 implements Runnable{
    public void run() {
        System.out.println("Multiple of 3:");
        for(int i=0;i<6;i++){
        System.out.println("Thread-B"+" "+i*3);
        }
    }
}
class Program2{
    public static void main(String args[])throws Exception{
        MyThread1 obj1 = new MyThread1();
        Thread t1 = new Thread(obj1);
        MyThread2 obj2 = new MyThread2();
        Thread t2 = new Thread(obj2);
        t1.start();
        t2.start();
        t1.setName("Thread-A");
        t2.setName("Thread-B");
        t2.join();
        System.out.println("Name of the first thread"+t1.getName());
        System.out.println("Name of the second thread"+" "+t2.getName());
        System.out.println("Priority of Thread-A"+" "+t1.getPriority());
        for(int j=0;j<6;j++){
            System.out.println("Main-Thread"+" "+j*1);
        }
    }
} 

